//auth controller
main.controller("AuthController", function ($scope, $http, $rootScope, $location) {
    $scope.user = {username: '', password: ''};
	$scope.error_message = '';
    $scope.login = function(){
		$http.post('/auth/login', $scope.user).success(function(data){
			if(data.state == 'success'){
				$rootScope.authenticated = true;
				$rootScope.current_user = data.user.username;
				$rootScope.sess = data.user;
				sessionStorage.setItem('current_user', $rootScope.sess.username);
				$location.path('/ask');
			}
			else{
				$scope.error_message = data.message;
				$rootScope.sess = null;
			}
		});
	};

	$scope.register = function(){
		$http.post('/auth/signup', $scope.user).success(function(data){
			if(data.state == 'success'){
				$rootScope.authenticated = true;
				$rootScope.current_user = data.user.username;
				$location.path('/');
			}
			else{
				$scope.error_message = data.message;
			}
		});
	};

	$scope.goToLogin = function(){
		$location.path('/');
	}
});