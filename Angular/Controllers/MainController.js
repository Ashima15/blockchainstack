﻿//main controller
main.controller("MainController", function ($scope, $http, chatSocket) {
  $scope.disableLoadMore = false;
  $scope.saveQuestion = function (user) {
    questionData = { 'userName' : user , 'question': $scope.question.question, 'detail' : $scope.question.detail};
    $http.post('/ask', questionData).success(function (data) {
      if (data.state == 'success') {
        chatSocket.emit('questionSave', {
          question: questionData
        });
        $scope.question.question = "";
        $scope.question.detail = "";
      }
    });
  };

  $http.get('ask').success(function (data) {
    $scope.showQuestions = data;
  }).error(function (data) {
    console.log('Error: ' + data);
  });

  $scope.limit = 4;

  $scope.loadMore = function () {
    $scope.limit = $scope.limit + 4;
    if($scope.limit >= $scope.showQuestions.length){
      $scope.disableLoadMore = true;
    }
  }

  $scope.changeState = function () {
    $scope.coloumnOne = true;
  }
  
  chatSocket.on('showQuestion', function(data){
    var element = {};
    element.question =  data.data.question.question;
    element.detail = data.data.question.detail;
    element.author = data.data.question.userName;
    element.created_at = data.created_at;
    $scope.showQuestions.push(element);
  })

});