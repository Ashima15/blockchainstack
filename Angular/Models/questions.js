let mongoose = require('mongoose');

let questionsSchema = mongoose.Schema({
    question: String,
    detail: String,
    author : {
        type : String,
        required : true
    },
    created_at: {
        type: Date, 
        default: Date.now
    }
});

let questions = module.exports = mongoose.model('questions' , questionsSchema);