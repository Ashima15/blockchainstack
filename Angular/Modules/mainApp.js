﻿//Angular Starter App
var main = angular.module("main", ['ui.router', 'ngRoute', 'ngResource', 'btford.socket-io'])
    .run(function ($http, $rootScope) {
        if (sessionStorage.length > 0) {
            $rootScope.current_user = sessionStorage.current_user;
            $rootScope.authenticated = true;
        } else {
            $rootScope.authenticated = false;
        }

        $rootScope.signout = function () {
            $http.get('auth/signout');
            $rootScope.authenticated = false;
            sessionStorage.clear();
        };
    });

//Routing Configuration (define routes)
main.config([
    '$stateProvider', '$urlRouterProvider', '$httpProvider',
    function ($stateProvider, $urlRouterProvider, $rootScope) {
        $urlRouterProvider.otherwise('/');
        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: 'Index.html',
                caseInsensitiveMatch: true,
                controller: 'AuthController'
            })
            .state('ask', {
                url: '/ask',
                caseInsensitiveMatch: true,
                controller: 'MainController',
                views: {
                    '': {
                        templateUrl: 'Ask.html',
                        controller: 'MainController',
                    },

                    'columnOne@ask': {
                        template: `
                             <div ng-show="authenticated" class="question-box">
                                <form method="POST" class="form-group">
                                    <h1>Ask a question</h1>
                                    <input type="text" name="question" ng-model="question.question" class="form-control"> <br>
                                    <textarea name="detail" cols="1000" rows="5" placeholder="Enter details" ng-model="question.detail" class="form-control"></textarea>
                                    <button type="submit" ng-click="saveQuestion()" class="btn btn-primary submit-question-btn">Submit</button>
                                </form>
                              </div>` ,
                        controller: 'MainController'
                    },


                }
            })
            .state('about', {
                url: '/about',
                templateUrl: 'About.html',
                caseInsensitiveMatch: true,
                controller: 'MainController'
            })
            .state('login', {
                url: '/login',
                templateUrl: 'login.html',
                caseInsensitiveMatch: true,
                controller: 'AuthController'
            })
            .state('register', {
                url: '/register',
                templateUrl: 'register.html',
                caseInsensitiveMatch: true,
                controller: 'AuthController'
            })
            .state('unauth', {
                url: '/unauth',
                templateUrl: 'unauth.html',
                caseInsensitiveMatch: true
            });
    }
]);
