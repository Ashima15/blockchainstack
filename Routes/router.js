var express = require('express');
var router = express.Router();
var mongoose = require( 'mongoose' );
let Questions = require('../Angular/Models/questions.js');
let User = require('../Angular/Models/user.js');



router.get('/ask' , function(req,res){
    Questions.find({}, function(err, questions){
        if(err){
            console.log(err);
        }
        else{
			res.json(questions)
        }
    })
});

router.post('/ask', function (req, res) {
    let questions = new Questions();
    questions.question = req.body.question;
    questions.detail = req.body.detail;
    questions.author = req.body.userName;
  
    questions.save(function (err) {
      if (err) {
        console.log(err);
        return;
      }
      else {
        res.send({ state: 'success' });
      }
    })
  });

module.exports = router;