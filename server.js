//adding opensource modules to application 
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var bcrypt = require('bcrypt-nodejs');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var session = require('express-session');
var mongoose = require('mongoose');
var models_user = require('./Angular/Models/user.js');
let Questions = require('./Angular/Models/questions.js');
var app = express();


var http = require('http').Server(app);
var io = require('socket.io')(http);

http.listen(process.env.PORT || 8080, function () {
  console.log('App listening at http://localhost:8080');
});

mongoose.connect('mongodb://localhost:27017/campk12');

// mongoose.connect('mongodb://Ashima:abcdxyz1234@ds153890.mlab.com:53890/campk12');


var router = require('./Routes/router');
var authenticate = require('./Routes/authentication')(passport);


app.set('views', path.join(__dirname, 'Views'));
app.set('view engine', 'ejs');

app.use(cookieParser());
app.use(logger('dev'));
app.use(session({
  secret: 'keyboard cat'
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(passport.initialize()); //initializing passport
app.use(passport.session()); //initializing passport session

app.use('/', router);
app.use('/auth', authenticate);
app.use(express.static(path.join(__dirname, 'scripts')));
app.use(express.static(path.join(__dirname, 'Content')));
app.use(express.static(path.join(__dirname, 'Angular')));
app.use(express.static(path.join(__dirname, 'Views/Main')));
app.use(express.static(path.join(__dirname, 'Views/Authentication')));
app.use(express.static(path.join(__dirname, 'public')));

io.on('connect', function (socket) {
  console.log('a user has connected');
  socket.on('disconnect', function () {
    console.log('a user has disconnected');
  })
  socket.on('questionSave', function(data){
    io.sockets.emit('showQuestion', {
      data: data,
      created_at: new Date()
    });
  })
})

var initPassport = require('./Passport/passport-init');
initPassport(passport);

router.get('/', function (req, res, next) {
  res.render('index');
});

